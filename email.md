## Emails

#### SocialMediaChecker
https://github.com/Fah4d/SocialMediaChecker
Check if an email is linked to Twitter/Instagram/Snapchat

#### ProtonMail accounts and ProtonVPN IP addresses
https://github.com/pixelbubble/ProtOSINT

#### MOSINT
It gathers info on target email
https://github.com/alpkeskin/mosint 

#### Aware-online.com

Email search tool : https://www.aware-online.com/osint-tools/e-mail-search-tool/

#### MAILCAT 

The only cat who can find existing email addresses by nickname.  
https://github.com/sharsil/mailcat
