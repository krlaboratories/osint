## Passwords 

https://github.com/beurtschipper/Depix # tool to recover passwords from pixelized screenshots


## Tools to get info on / same images 

https://imgops.com  
https://same.energy

## Storage 

https://github.com/nicomda/InfiniDrive # Not really OSINT but how to store images on Amazon for free 
